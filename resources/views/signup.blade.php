@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/signup.css' />
@endsection

@section('content')
    <h1 class='title'>S'inscrire</h1>

    <form action='/signup' method='POST'>
        Utilisateur : <input type='text' name='username' class='form-control form-field' placeholder='Utilisateur' />
        Mot de passe : <input type='password' name='password' class='form-control form-field' placeholder='Mot de passe' />
        Email : <input type='email' name='email' class='form-control form-field' placeholder='Email' />

        <input type="hidden" name='_token' value='{{csrf_token()}}' />
        <input type="submit" class="btn btn-block btn-primary signup-submit" value="Créer son compte"/>
        <p class='login-prompt text-center'>
            <small>Déjà inscrit·e ? <a href='{{route('login')}}'>S'identifier</a></small>
        </p>
    </form>
@endsection

@section('js')
@endsection
