@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/lost_password.css' />
@endsection

@section('content')
<h1 class='title'>Changer le mot de passe</h1>

<form action='/lost_password' method='POST'>
    <input type='email' name='email' placeholder='Email' class='form-control email-input-pd'>
    <input type="hidden" name='_token' value='{{csrf_token()}}' />
    <input type='submit' value='Demander le changement de mot de passe par mail' class='btn btn-block btn-primary'>
</form>

@endsection
