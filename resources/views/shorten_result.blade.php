@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/shorten_result.css' />
@endsection

@section('content')
<h1>Félicitations !</h1>
Votre lien raccourci a bien été créé.<br />Vous pouvez le copier <kbd>ctrl+C</kbd> / coller <kbd>ctrl+V</kbd> :<br /><br />
<div class="input-group">
    <input type='text' class='result-box form-control' readonly value='{{$short_url}}' id='short_url' />
    <span class="input-group-btn">
        <a class="btn btn-primary" href="#" id='clipboard-copy' data-clipboard-target='#short_url' data-toggle='tooltip' data-placement='bottom' data-title='Copié!'><i class='fa fa-clipboard' aria-hidden='true' ></i> Copier</a>
    </span>
</div>
<hr />
Par ailleurs, clic droit -> "enregistrer l'image sous" ou fashez le QrCode ci dessous avec votre téléphone.
<div class="qr-code-container"></div>
<a href="{{ route('index') }}" class='btn btn-lg btn-primary'>Raccourcir un autre lien</a>



@endsection


@section('js')
<script src='/js/qrcode.min.js'></script>
<script src='/js/clipboard.min.js'></script>
<script src='/js/shorten_result.js'></script>
@endsection
