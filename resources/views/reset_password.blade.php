@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/reset_password.css' />
@endsection

@section('content')
<h1 class='header'>Changer le mot de passe</h1>
<form action="#" method='POST'>
    <input type='password' id='passwordFirst' placeholder='Nouveau mot de passe' class='form-control password-input-pd'>
    <input type='password' id='passwordConfirm' placeholder='Répéter le mot de passe' class='form-control password-input-pd' name='new_password'>

    <input type="hidden" name='_token' value='{{csrf_token()}}' />
    <input type='submit' id='submitForm' value='Changer le mot de passe' class='btn btn-blockbtn-primary'>
</form>
@endsection

@section('js')
<script src='/js/reset_password.js'></script>
@endsection
