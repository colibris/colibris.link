@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='css/login.css' />
@endsection

@section('content')
<h1 class='title'>S'identifier</h1>
<form action="login" method="POST">
Utilisateur : <input type="text" placeholder="Utilisateur" name="username" class="form-control login-field" />
Mot de passe : <input type="password" placeholder="Mot de passe" name="password" class="form-control login-field" />
    <input type="hidden" name='_token' value='{{csrf_token()}}' />
    <input type="submit" value="S'identifier" class="login-submit btn btn-block btn-primary" />

    <p class='login-prompts'>
    @if (env('POLR_ALLOW_ACCT_CREATION') == true)
        <small>Pas de compte ? <a href='{{route('signup')}}'>S'inscrire</a></small>
    @endif

    @if (env('SETTING_PASSWORD_RECOV') == true)
        <small>Mot de passe perdu ? <a href='{{route('lost_password')}}'>Le récupérer par mail</a></small>
    @endif
    </p>
</form>
@endsection
