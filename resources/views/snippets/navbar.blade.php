<nav role="navigation" class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('index') }}" title="{{env('APP_NAME')}}">
                <img src="{{ url('/') }}/img/logo-Colibris-link.svg" alt="logo" class="logo" />
            </a>
            <a href="https://www.jedonneenligne.org/colibris/OUTILSLIBRES/" title="Merci de votre soutien!" class="btn btn-donate btn-sm btn-danger"><i class="fa fa-heart"></i> <span class="faire-un">Faire un </span><span class="don">don</span></a>
        </div>

        <div id="navbar-collapse" class="collapse navbar-collapse">
            <ul id="navbar" class="nav navbar-nav navbar-right">
            @if (env('USE_CAS_AUTH')) 
                @if (empty(session('username')))
                    <li><a class="cas-login" href="{{ route('cas/login') }}">S'identifier</a></li>
                @else
                
                <li><a href="{{ route('admin') }}">Tableau de bord</a></li>
                <li><a href="{{ route('cas/logout') }}" title="Se déconnecter du compte {{session('username')}}"><i class="glyphicon glyphicon-log-out"></i> Se déconnecter</a></li>
                @endif
            @else
                @if (empty(session('username')))
                    @if (env('POLR_ALLOW_ACCT_CREATION'))
                        <li><a href="{{route('signup')}}">S'inscrire</a></li>
                    @endif

                    <li><a href="{{ route('login') }}">S'identifier</a></li>
                @else
                
                <li><a href="{{ route('admin') }}"><i class="glyphicon glyphicon-list-alt"></i> Tableau de bord</a></li>
                <li><a href="{{ route('logout') }}" title="Se déconnecter du compte {{session('username')}}"><i class="glyphicon glyphicon-log-out"></i> Se déconnecter</a></li>
                @endif
            @endif
            </ul>
        </div>
    </div>
</nav>
