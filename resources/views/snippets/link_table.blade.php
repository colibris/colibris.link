<table id="{{$table_id}}" class="table table-hover">
    <thead>
        <tr>
            <th>Lien court</th>
            <th>Lien long</th>
            <th>Clics</th>
            <th>Date</th>
            @if ($table_id == "admin_links_table")
            {{-- Show action buttons only if admin view --}}
            <th>Créateur</th>
            <th>Bloquer</th>
            <th>Effacer</th>
            @endif
        </tr>
    </thead>
</table>
