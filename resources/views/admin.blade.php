@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/admin.css'>
<link rel='stylesheet' href='/css/datatables.min.css'>
@endsection

@section('content')
<div ng-controller="AdminCtrl" class="ng-root">
    <h1>Tableau de bord de {{session('username')}}</h1>
    <ul class='nav nav-tabs admin-nav' role='tablist'>
        <li role='presentation' aria-controls="links" class='admin-nav-item active'><a href='#links'>Mes liens</a></li>
        @if ($role == $admin_role)
        <li role='presentation' class='admin-nav-item'><a href='#admin-links'>Gérer les liens</a></li>
        <li role='presentation' class='admin-nav-item'><a href='#admin-users'>Gérer les utilisateurs</a></li>
        @if ($api_active == 1)
        <li role='presentation' class='admin-nav-item'><a href='#developer'>Développeur</a></li>
        @endif
        @endif
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="links">
            <a href="{{ route('index') }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Nouveau lien</a>
            <h3>Mes liens</h3>
            @include('snippets.link_table', [
                'table_id' => 'user_links_table'
            ])
        </div>



        @if ($role == $admin_role)
        <div role="tabpanel" class="tab-pane" id="admin-links">
            <a href="{{ route('index') }}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Nouveau lien</a>
            <h3>Liens</h3>
            @include('snippets.link_table', [
                'table_id' => 'admin_links_table'
            ])
        </div>

        <div role="tabpanel" class="tab-pane" id="admin-users">
            <a ng-click="state.showNewUserWell = !state.showNewUserWell" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Nouvel utilisateur</a>
            <h3>Utilisateurs</h3>

            <div ng-if="state.showNewUserWell" class="new-user-fields well">
                <table class="table">
                    <tr>
                        <th>Utilisateur</th>
                        <th>Mot de passe</th>
                        <th>Email</th>
                        <th>Rôle</th>
                        <th></th>
                    </tr>
                    <tr id="new-user-form">
                        <td><input type="text" class="form-control" ng-model="newUserParams.username"></td>
                        <td><input type="password" class="form-control" ng-model="newUserParams.userPassword"></td>
                        <td><input type="email" class="form-control" ng-model="newUserParams.userEmail"></td>
                        <td>
                            <select class="form-control new-user-role" ng-model="newUserParams.userRole">
                                @foreach  ($user_roles as $role_text => $role_val)
                                    <option value="{{$role_val}}">{{$role_text}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <a ng-click="addNewUser($event)" class="btn btn-primary btn-sm status-display new-user-add">Ajouter</a>
                        </td>
                    </tr>
                </table>
            </div>

            @include('snippets.user_table', [
                'table_id' => 'admin_users_table'
            ])

        </div>
        @endif

        @if ($api_active == 1)
        <div role="tabpanel" class="tab-pane" id="developer">
            <h3>Développeur</h3>

            <p>Clé d'API et documentation.</p>
            <p>
                Documentation:
                <a href='http://docs.polr.me/en/latest/developer-guide/api/'>http://docs.polr.me/en/latest/developer-guide/api/</a>
            </p>

            <h4>Clé d'API : </h4>
            <div class='row'>
                <div class='col-md-8'>
                    <input class='form-control status-display' disabled type='text' value='{{$api_key}}'>
                </div>
                <div class='col-md-4'>
                    <a href='#' ng-click="generateNewAPIKey($event, '{{$user_id}}', true)" id='api-reset-key' class='btn btn-danger'>Changer</a>
                </div>
            </div>


            <h4>API Quota: </h4>
            <h2 class='api-quota'>
                @if ($api_quota == -1)
                    illimité
                @else
                    <code>{{$api_quota}}</code>
                @endif
            </h2>
            <span> requetes par minute</span>
        </div>
        @endif
    </div>
</div>

<div class="angular-modals">
    <edit-long-link-modal ng-repeat="modal in modals.editLongLink" link-ending="modal.linkEnding"
        old-long-link="modal.oldLongLink" clean-modals="cleanModals"></edit-long-link-modal>
    <edit-user-api-info-modal ng-repeat="modal in modals.editUserApiInfo" user-id="modal.userId"
        api-quota="modal.apiQuota" api-active="modal.apiActive" api-key="modal.apiKey"
        generate-new-api-key="generateNewAPIKey" clean-modals="cleanModals"></edit-user-api-info>
</div>

@endsection

@section('js')
{{-- Include modal templates --}}
@include('snippets.modals')

{{-- Include extra JS --}}
<script src='/js/datatables.min.js'></script>
<script src='/js/api.js'></script>
<script src='/js/AdminCtrl.js'></script>
@endsection
