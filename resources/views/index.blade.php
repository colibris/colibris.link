@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='css/index.css' />
@endsection

@section('content')
<h1 class='title'>Raccourcir un lien</h1>

<form method='POST' action='/shorten' role='form'>
    <div class="input-group input-group-lg">
        <input type='url' autocomplete='off' class='form-control long-link-input' placeholder='https://' name='link-url' autofocus />
        <span class="input-group-btn">
            <input class="btn btn-primary" type="submit" value="GO!" />
        </span>
    </div>
    
    <a href='#' id='show-link-options'>Personnaliser le lien court</a>

    <div class='row' id='options' ng-cloak>
        @if (!env('SETTING_PSEUDORANDOM_ENDING'))
        {{-- Show secret toggle only if using counter-based ending --}}
        <div class='btn-group btn-toggle visibility-toggler' data-toggle='buttons'>
            <label class='btn btn-primary btn-sm active'>
                <input type='radio' name='options' value='p' checked /> Public
            </label>
            <label class='btn btn-sm btn-default'>
                <input type='radio' name='options' value='s' /> Secret
            </label>
        </div>
        @endif

        <div>
            <div class='custom-link-text'>
                <h2 class='site-url-field'>{{env('APP_ADDRESS')}}/</h2>
                <input type='text' autocomplete="off" class='form-control custom-url-field' name='custom-ending' />
            </div>
            <div>
                <a href='#' class='btn btn-default btn-xs check-btn' id='check-link-availability'>Vérifier la disponibilité du lien</a>
                <div id='link-availability-status'></div>
            </div>
        </div>
    </div>

    <input type="hidden" name='_token' value='{{csrf_token()}}' />
</form>
@endsection

@section('js')
<script src='js/index.js'></script>
@endsection
