@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/about.css' />
<link rel='stylesheet' href='/css/effects.css' />
@endsection

@section('content')
<h1>A propos</h1>
@if ($role == "admin")
<dl>
    <dt>Application installée: {{env('APP_NAME')}} sur {{env('APP_ADDRESS')}}, généré le {{env('POLR_GENERATED_AT')}}<dt>
</dl>
@endif

<p>
    <b>{{env('APP_NAME')}}</b> est un service pour raccourcir les URLs et personnaliser le nom de ce lien, afin de pouvoir les partager sur des formats imprimés ou web, ou pouvoir donner le lien par oral plus facilement, et aussi bénéficier d'un QRcode.<br /><br />
    Pour les personnes identifiées, il est possible d'avoir des statistiques de visites pour leurs liens créés.<br /><br />
</p>
<p>
    <div class='well logo-well pull-right'>
        <img class='logo-img' src='/img/logo.png' />
    </div>
    <b>{{env('APP_NAME')}}</b> utilise le logiciel libre Polr. (<a href="https://framagit.org/colibris/colibris.link" title="Modifications de code pour colibris.link">source</a>)<br />
    Polr est sous licence GNU GPL.<br />
    Auteur principal : Chaoyi Zha<br />
    En savoir plus sur <a href='https://github.com/Cydrobolt/polr'>la page Github</a> ou <a href="https//project.polr.me">le site du projet</a>.<br />
</p>
@endsection