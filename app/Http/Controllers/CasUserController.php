<?php
namespace App\Http\Controllers;

use Mail;
use Hash;
use App\Models\User;
use Illuminate\Http\Request;

use App\Helpers\CryptoHelper;
use App\Helpers\UserHelper;

use App\Factories\UserFactory;
use phpCAS;

class CasUserController extends Controller
{
    /**
     * Handle an login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function login(Request $request)
    {
        phpCAS::client(CAS_VERSION_2_0, getenv('CAS_HOST'), 443, getenv('CAS_CONTEXT'));
        phpCAS::setNoCasServerValidation();

        if (phpCAS::isAuthenticated()) {
            $username = phpCAS::getUser();
            if (!UserHelper::userExists($username)) {
                $api_active = false;
                $api_key = null;
        
                if (env('SETTING_AUTO_API')) {
                    // if automatic API key assignment is on
                    $api_active = 1;
                    $api_key = CryptoHelper::generateRandomHex(env('_API_KEY_LENGTH'));
                }
                $admins = getenv('CAS_ADMINS');
                $admins = explode(',', $admins);
                $admins = array_map('trim', $admins);
                $role = in_array($username, $admins) ? 'admin' : '';
                $attr = phpCAS::getAttributes();
                $password = bin2hex(random_bytes(15));
                $user = UserFactory::createUser($username, $attr['mail'], $password, 1, $request->ip(), $api_key, $api_active, $role);
            } else {
                $user = User::where('active', 1)
                ->where('username', $username)
                ->first();
    
                if ($user == null) {
                    return false;
                }
            }


            $request->session()->put('username', $username);
            $request->session()->put('role', $user->role);
            return redirect()->route('admin');
        } else {
            phpCAS::forceAuthentication();
        }
    }

    /**
     * Handle an login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        if (getenv('CAS_LOGOUT_FROM_CAS')) {
            phpCAS::client(CAS_VERSION_2_0, getenv('CAS_HOST'), 443, getenv('CAS_CONTEXT'));
            phpCAS::setNoCasServerValidation();
            phpCAS::logout();
        }
        $request->session()->forget('username');
        $request->session()->forget('role');
        return redirect()->route('index');
    }
}
